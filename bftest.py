#!/usr/bin/python3
from pybatfish.client.commands import *
from pybatfish.question.question import load_questions
from pybatfish.datamodel.flow import (HeaderConstraints,
                                         PathConstraints)
from pybatfish.question import bfq
import random
import sys

NETWORK_NAME = "test"
SNAPSHOT_NAME = "test"
SNAPSHOT_PATH = "./intended"
BATFISH_SERVICE_IP = "localhost"

bf_session.host = BATFISH_SERVICE_IP
load_questions()

bf_init_snapshot(SNAPSHOT_PATH, name=SNAPSHOT_NAME, overwrite=True)

print("******** Showing all of the Interfaces which are connected ********")
int_check = bfq.interfaceProperties(properties="Active").answer()
print(int_check)

#Check to show all of the edge connected services 
print("******** Showing all of the edge connected servivces ********")
edge_services = bfq.bgpEdges().answer().frame()
print(edge_services)

#Check for peerings peerings
print("******** Showing BGP peering sessions for all nodes ********")
bgp_peers = bfq.bgpSessionStatus(nodes='/Leaf/').answer().frame()
print(bgp_peers)

