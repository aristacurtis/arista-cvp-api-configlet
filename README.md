## arista-cvp-api-configlet

Ansible Playbook for using CloudVision Portal API, galaxy collections, to generate per device dynamic configlets and assigning to devices based on container membership


# How To Use

## clone repository

```bash
$ git clone https://gitlab.com/aristacurtis/arista-cvp-api-configlet.git
$ cd arista-cvp-api-configlet
```

## modify relevant variables
  - inventory.ini | `change ansible_host, ansible_user, ansible_password variables`
  - playbook.configlet.yaml | `change update_container, add_buildername variables`
  - **if** using secrets.yml *_(encrypted vault)_*, create appropriate secrets.yml file and encrypt it:
    ```bash
    $ echo "vault_ansibleuser: \"cvpusername\"" > secrets.yml
    $ echo "vault_ansiblepass: \"cvppassword\"" > secrets.yml
    $ ansible-vaule encrypt secrets.yml
    ```

## run it

```bash
ansible-playbook -i inventory.ini --ask-vault-pass playbook.configletbuilder.yaml
```
### what it does

1.  uses _arista.cvp.cv_facts_ collection module to connect to CVP API and collect facts (container topology, devices, configlets, etc) from CVP
2.  pre-processes data to identify target switches (all devices members of `update_container`) for configlet generation from `add_builder`  dymaic configlet
3.  generates per switch configlets and saves to `intended/configs/*.txt` static files
      * Insert test cases for validating static configurations
4.  compares all applied configlets (pre-existing + newly generated from this playbook) per device against running configuration, returns:
      * new / mismatch / reconcile lines count (only number of each, not full configuration diffs)
5.  if no new / mismatch / reconcile lines _or_ `validate` flag set to False, end play:  nothing to be done
6.  if work to be done, add `Temporary Action` ot bind configlets to devices, then `Save Proposed Tology` in order to generate configuration Tasks, per device
7.  return Task IDs of generated tasks (_do not EXECUTE tasks_)


## dependencies

This playbook requires the following to be installed on the Ansible control machine:
   -  python 3.7
   -  ansible >= 2.9.0
   -  requests >= 2.22.0
   -  treelib version 1.5.5
      * can be installed with:
```bash 
$ pip3 install -r requirements.txt
```

Additionally, the <b>arista.cvp</b> collection needs to be installed from galaxy:

```bash
$ ansible-galaxy collection install arista.cvp
```
