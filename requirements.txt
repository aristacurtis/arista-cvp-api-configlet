#python 3.7
ansible >= 2.9.0
requests >= 2.22.0
treelib >=1.5.5
paramiko >= 2.7.1
pybatfish >= 2020.4.23
